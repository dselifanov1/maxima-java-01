package org.example;

public class App {


    public static void main(String[] args) {

                System.out.println(maxDigitsSumPosition(9999, 999, 999));

    }


    public static byte maxDigitsSumPosition(int... arr) {


        int sum;
        int i = 0;
        int maxInd = 0;
        int max = -1;

        for (; i < arr.length; i++) {

            sum = 0;
             while (arr[i] > 0) {
                sum += arr[i] % 10;
                arr[i] /= 10;}

                System.out.println(sum);
                 if (sum >= max && i >= maxInd) {
                     max = sum;
                     maxInd = i;
                 }


        }
        return (byte) maxInd;
    }
}